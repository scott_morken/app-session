<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 12:32 PM
 */

namespace Smorken\Session;


class SessionHandler {

    const SESSION_STARTED = TRUE;
    const SESSION_NOT_STARTED = FALSE;

    // The state of the session
    protected $sessionState = self::SESSION_NOT_STARTED;

    /**
     * @var \Smorken\Session\Adapter\AdapterInterface
     */
    protected $adapter;

    /**
     * Instantiate the SessionHandler with the provider session adapter
     * @param Adapter\AdapterInterface $adapter
     * @throws SessionException
     */
    public function __construct(\Smorken\Session\Adapter\AdapterInterface $adapter)
    {
        $this->initAdapter($adapter);
    }

    /**
     * @param \Smorken\Session\Adapter\AdapterInterface $adapter
     * @throws SessionException
     */
    protected function initAdapter($adapter)
    {
        if ($adapter && !$this->adapter) {
            $this->adapter = $adapter;
        }
        if (!$this->adapter instanceof \Smorken\Session\Adapter\AdapterInterface) {
            throw new \Smorken\Session\SessionException("A session adapter is required.");
        }
    }

    /**
     * @return Adapter\AdapterInterface
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * Starts/restarts the session, initializes the session adapter
     * if passed in to ::getInstance
     * @return bool
     */
    public function startSession()
    {
        if ( $this->sessionState == self::SESSION_NOT_STARTED )
        {
            $this->sessionState = $this->adapter->startSession();
        }

        return $this->sessionState;
    }

    /**
     * Returns the session from the adapter
     * @return mixed
     */
    public function getSession()
    {
        return $this->adapter->getSession();
    }

    /**
     * regenerates the session, may not do anything on some adapters
     */
    public function regenerate()
    {
        $this->startSession();
        $this->adapter->regenerate();
    }

    /**
     * Clears any session data
     * calls adapter::clear()
     */
    public function clear()
    {
        if ($this->sessionState == self::SESSION_STARTED) {
            $this->adapter->clear();
        }
    }

    public function flash($key, $message)
    {
        $flash = $this->adapter->flash;
        if (!$flash) {
            $flash = array();
        }
        $flash[$key][] = $message;
        $this->adapter->flash = $flash;
    }

    public function getFlash()
    {
        if (!$this->adapter->flash) {
            return array();
        }
        $messages = $this->adapter->flash;
        $this->adapter->flash = array();
        return $messages;
    }

    /**
     * Magic method to set a value on the session adapter
     * @param $name Key of value
     * @param $value data to store
     */
    public function __set($name, $value)
    {
        $this->startSession();
        $this->adapter->$name = $value;
    }

    /**
     * Magic method to get a value on the session adapter
     * @param $name key to retrieve
     * @return mixed data retrieved from session adapter
     */
    public function __get($name)
    {
        $this->startSession();
        return $this->adapter->$name;
    }

    /**
     * Checks if a key is set on the session adapter
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        $this->startSession();
        return isset($this->adapter->$name);
    }

    /**
     * Unsets a key on the session adapter
     * @param $name
     */
    public function __unset($name)
    {
        $this->startSession();
        unset($this->adapter->$name);
    }


    /**
     * Destroys the current session
     * @return bool true if session deleted, false if destroy fails
     */
    public function destroy()
    {
        $this->startSession();
        $this->adapter->clear();
        $this->sessionState = $this->adapter->destroy();
        return $this->sessionState;
    }

}