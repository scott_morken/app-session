<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 12:38 PM
 */

namespace Smorken\Session\Adapter;


interface AdapterInterface {

    /**
     * Configuration options for the adapter
     * @param array $config
     */
    public function setConfig($config = array());

    /**
     * (Re)starts the session.
     *
     * @return bool TRUE if the session has been initialized, else FALSE.
     **/
    public function startSession();

    /**
     * Returns the session data
     * @return mixed
     */
    public function getSession();

    /**
     * Regenerate session
     * @return null
     */
    public function regenerate();

    /**
     * clears all session related data, calls ::destroy
     * @return null
     */
    public function clear();

    /**
     * utility function to merge namespace and name
     * @param $name
     * @return string
     */
    public function getNameWithNamespace($name);

    /**
     * Sets the namespace prefix, use with set and get
     * @param $namespace
     * @return null
     */
    public function setNamespace($namespace);

    /**
     * @return string
     */
    public function getNamespace();

    /**
     * Magic method to set a value
     * @param $name
     * @param $value
     * @return null
     */
    public function __set( $name , $value );

    /**
     * Magic method to get a value
     * @param $name
     * @return mixed
     */
    public function __get( $name );

    /**
     * Magic method to check if a key exists
     * @param $name
     * @return bool
     */
    public function __isset( $name );

    /**
     * Magic method to unset a key
     * @param $name
     * @return null
     */
    public function __unset( $name );

    /**
     * destroys session
     * @return null
     */
    public function destroy();
} 