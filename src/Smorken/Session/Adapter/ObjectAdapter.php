<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 12:38 PM
 */

namespace Smorken\Session\Adapter;

/**
 * Class ObjectAdapter
 * @package Smorken\Session\Adapter
 *
 * Dummy adapter for testing, uses an array to store the session data
 */
class ObjectAdapter implements AdapterInterface {

    protected $session;

    protected $namespace;

    /**
     * (Re)starts the session.
     *
     * @return bool TRUE if the session has been initialized, else FALSE.
     **/
    public function startSession()
    {
        $this->session = array();
        return true;
    }

    /**
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Regenerate session
     * @return null
     */
    public function regenerate()
    {

    }

    /**
     * clears all session related data, calls ::destroy
     * @return null
     */
    public function clear()
    {
        $this->session = array();
    }

    /**
     * @param $name
     * @param $value
     * @return null
     */
    public function __set($name, $value)
    {
        $name = $this->getNameWithNamespace($name);
        $this->session[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        $name = $this->getNameWithNamespace($name);
        return array_key_exists($name, $this->session) ? $this->session[$name] : null;
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        $name = $this->getNameWithNamespace($name);
        return array_key_exists($name, $this->session);
    }

    /**
     * @param $name
     * @return null
     */
    public function __unset($name)
    {
        $name = $this->getNameWithNamespace($name);
        unset($this->session[$name]);
    }

    /**
     * destroys session
     * @return null
     */
    public function destroy()
    {
        $this->session = null;
        return $this->session === null;
    }

    /**
     * Sets the namespace prefix, use with set and get
     * @param $namespace
     * @return null
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * utility function to merge namespace and name
     * @param $name
     * @return string
     */
    public function getNameWithNamespace($name)
    {
        return ($this->getNamespace() ? $this->getNamespace() . '.' : '') . $name;
    }

    /**
     * Configuration options for the adapter
     * @param array $config
     */
    public function setConfig($config = array())
    {
        // TODO: Implement setConfig() method.
    }
}