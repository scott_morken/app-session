<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 12:39 PM
 */

namespace Smorken\Session\Adapter;

/**
 * Class SessionAdapter
 * @package Smorken\Session\Adapter
 *
 * Adapter for the standard PHP session
 */
class SessionAdapter implements AdapterInterface {

    /**
     * Session namespace
     * @var string
     */
    protected $namespace;

    protected $config = array();

    public function __construct(array $config = array())
    {
        $this->setConfig($config);
        $this->initConfig();
    }

    /**
     * close the session so it is available for the next request
     */
    public function __destruct()
    {
        if (isset($_SESSION) && $_SESSION) {
            session_write_close();
        }
    }

    public function setConfig($config = array())
    {
        $this->config = $config;
    }

    protected function initConfig()
    {
        $defaults = array('httponly' => false, 'secure' => false, 'path' => '/', 'domain' => '');
        $options = array_merge($defaults, $this->config);
        foreach($options as $k => $v) {
            $name = sprintf('session.cookie_%s', $k);
            ini_set($name, $v);
        }
    }

    /**
     * (Re)starts the session.
     *
     * @return bool TRUE if the session has been initialized, else FALSE.
     **/
    public function startSession()
    {
        if (!session_id()) {
            return session_start();
        }
        return true;
    }

    /**
     * returns the session from $_SESSION
     * @return mixed
     */
    public function getSession()
    {
        return $_SESSION;
    }

    /**
     * Regenerate session
     * @return null
     */
    public function regenerate()
    {
        session_regenerate_id(true);
    }

    /**
     * clears all session related data, calls ::destroy
     * @return null
     */
    public function clear()
    {
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
    }

    /**
     * Magic set, adds namespace to name if it is set
     * @param $name
     * @param $value
     * @return null
     */
    public function __set($name, $value)
    {
        $_SESSION[$this->getNameWithNamespace($name)] = $value;
    }

    /**
     * Magic get, gets from session (with namespace if set)
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        $name = $this->getNameWithNamespace($name);
        if (isset($_SESSION[$name]))
        {
            return $_SESSION[$name];
        }
    }

    /**
     * Checks if $name exists (adds namespace if set)
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        $name = $this->getNameWithNamespace($name);
        return isset($_SESSION[$name]);
    }

    /**
     * Unsets $name from $_SESSION (adds namespace if set)
     * @param $name
     * @return null
     */
    public function __unset($name)
    {
        $name = $this->getNameWithNamespace($name);
        unset($_SESSION[$name]);
    }

    /**
     * destroys session
     * @return null
     */
    public function destroy()
    {
        $this->clear();
        $sessionState = !session_destroy();
        unset($_SESSION);
        session_write_close();
        return !$sessionState;
    }

    /**
     * Sets the namespace prefix, use with set and get
     * @param $namespace
     * @return null
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * Returns the namespace
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * utility function to merge namespace and name
     * @param $name
     * @return string
     */
    public function getNameWithNamespace($name)
    {
        return ($this->getNamespace() ? $this->getNamespace() . '.' : '') . $name;
    }

} 