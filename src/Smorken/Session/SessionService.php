<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 12:40 PM
 */

namespace Smorken\Session;

use Smorken\Service\Service;

class SessionService extends Service {

    public function start()
    {
        $this->name = 'session';
    }

    /**
     * Binds SessionHandler with the adapter from session.adapter into 'session'
     * If you want to modify the session adapter, create config/session.php and add an adapter key
     * specifying the class to load that implements AdapterInterface
     */
    public function load()
    {
        $this->app->instance($this->getName(), function($c) {
            $adapter = $c['config']->get('session.adapter', 'Smorken\Session\Adapter\SessionAdapter');
            $options = $c['config']->get('session.options', array());
            return new SessionHandler(new $adapter($options));
        });
    }

} 