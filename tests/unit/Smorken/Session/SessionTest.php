<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:45 AM
 */

use Smorken\Session\SessionHandler;
use Smorken\Session\Adapter\ObjectAdapter;

class SessionTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var SessionHandler
     */
    protected $sut;

    public function setUp()
    {
        $this->createSUT();
    }

    /**
     * @return SessionHandler
     */
    public function createSUT()
    {
        $adapter = new ObjectAdapter();
        $this->sut = new Smorken\Session\SessionHandler($adapter);
    }

    public function testGetSessionIsArray()
    {
        $this->sut->startSession();
        $this->assertEquals(array(), $this->sut->getSession());
    }

    public function testClearSession()
    {
        $this->sut->startSession();
        $this->sut->clear();
        $this->assertEquals(array(), $this->sut->getSession());
    }

    public function testMagicSetAndGetSession()
    {
        $this->sut->foo = 'bar';
        $this->assertEquals('bar', $this->sut->foo);
    }

    public function testMagicIsset()
    {
        $this->sut->foo = 'bar';
        $this->assertTrue(isset($this->sut->foo));
    }

    public function testMagicUnset()
    {
        $this->sut->foo = 'bar';
        unset($this->sut->foo);
        $this->assertNull($this->sut->foo);
    }

    public function testDestroy()
    {
        $this->sut->startSession();
        $this->assertTrue($this->sut->destroy());
        $this->assertNull($this->sut->getSession());
    }

    public function testEmptyFlashReturnsArray()
    {
        $this->sut->startSession();
        $this->assertEquals(array(), $this->sut->getFlash());
    }

    public function testFlashSetsAsArrayKey()
    {
        $this->sut->startSession();
        $this->sut->flash('foo', 'bar');
        $this->assertArrayHasKey('foo', $this->sut->getFlash());
    }

    public function testFlashSetMultipleAddsToArray()
    {
        $this->sut->startSession();
        $this->sut->flash('foo', 'bar');
        $this->sut->flash('foo', 'biz');
        $flash = $this->sut->getFlash();
        $this->assertEquals(array('bar', 'biz'), $flash['foo']);
    }
}
 