This project borrows heavily from the excellent [Laravel 4](http://laravel.com) and [Symfony 2](http://symfony.com) projects.

## License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

## Standalone
~~~~
<?php
include 'vendors/autoload.php';
$adapter = new \Smorken\Session\Adapter\SessionAdapter();
$handler = new \Smorken\Session\SessionHandler($adapter);
~~~~

## Part of simple app

Should automatically be included if using composer to set up the project

Otherwise add a service line to config/app.php services array

```
'Smorken\Session\SessionService'
```